package Globalcare_Framework.TestComponents;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.io.FileHandler;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import Globalcare_Framework.Pages.LoginPage;
import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseTest {

public   WebDriver driver;
protected static Properties properties;
LoginPage loginpage=new LoginPage(driver);

public BaseTest() {
	this.properties = loadProperties(System.getProperty("user.dir")+"//src//main//resources//config//config.properties");
}

private static final String configPATH=System.getProperty("user.dir")+"//src//main//resources//ExcelData//Testdata.xlsx";
private static void getPropertys()
{
	properties =new Properties();
	try(FileInputStream input=new FileInputStream(configPATH)){
		properties.load(input);
	}catch(Exception e) {
		e.printStackTrace();
	}
	}
public static String getproper(String key) {
	if(properties==null) {
		getPropertys();
	}
	return properties.getProperty(key);
}

@BeforeMethod(alwaysRun=true)
public WebDriver initializeDriver() {
	
	String browsername=properties.getProperty("BROWSER_TYPE");
	if (driver==null) {
		if(browsername.equalsIgnoreCase("chrome")) {
			ChromeOptions option=new ChromeOptions();
			WebDriverManager.chromedriver().setup();
			driver=new ChromeDriver(option);
		}else if(browsername.equalsIgnoreCase("firefox"))
		{
			WebDriverManager.firefoxdriver().setup();
			driver=new FirefoxDriver();
		}else if(browsername.equalsIgnoreCase("edge"))
		{
			WebDriverManager.edgedriver().setup();
			driver=new EdgeDriver();
		}
	}
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.manage().window().maximize();
		return driver;
}

@AfterMethod(alwaysRun=true)
public void tearDown()
{
	driver.close();
	
}
private Properties loadProperties(String filePath) {
	Properties prop=new Properties();
	try {
		FileInputStream fis=new FileInputStream(filePath);
				prop.load(fis);
	}catch(Exception e){
		e.printStackTrace();
	}
	return prop;
}
public String getProperty(String key)
{
	return properties.getProperty(key);
}
protected static final String filePath=System.getProperty("user.dir")+"//src//main//resources//ExcelData//Testdata.xlsx";
protected static final String sheetname="Sheet1";
protected static final String sheet="Sheet2";

public String getScreenshot(String testcaseName,WebDriver driver) throws IOException {
	TakesScreenshot ts=(TakesScreenshot)driver;
	File source=ts.getScreenshotAs(OutputType.FILE);
	File file=new File(System.getProperty("user.dir") + "//reports//" + testcaseName + ".png");
	FileUtils.copyFile(source, file);
	return System.getProperty("user.dir")+ "//reports//" + testcaseName + ".png";
}

public static void captureScreenshot(WebDriver driver, String outputPath, String fileName) {

    try {
           TakesScreenshot ts = (TakesScreenshot) driver;
           
        File source = ts.getScreenshotAs(OutputType.FILE);
              new File(outputPath).mkdirs();
               String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
              String filePath = outputPath + File.separator + fileName + "_" + timestamp + ".png";
              FileHandler.copy(source, new File(filePath));

        System.out.println("Screenshot captured: " + filePath);

    } catch (IOException e) {

        e.printStackTrace();

    }

}
}





