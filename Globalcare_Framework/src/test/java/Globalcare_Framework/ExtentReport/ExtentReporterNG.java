package Globalcare_Framework.ExtentReport;



import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.time.Duration;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentReports;

import com.aventstack.extentreports.reporter.ExtentSparkReporter;

import Globalcare_Framework.TestComponents.BaseTest;
import Globalcare_Framework.abstractComponents.AbstractComponent;

public class ExtentReporterNG extends BaseTest{
	WebDriver driver;

	
	
	public static ExtentReports getReportObject() {
		String path=System.getProperty("user.dir")+"//reports//index.html";
		ExtentSparkReporter reporter=new ExtentSparkReporter(path);
		reporter.config().setReportName("Web Automation Results");
		reporter.config().setDocumentTitle("Test Execution Results");
		ExtentReports extent=new ExtentReports();
		extent.attachReporter(reporter);
		//extent.setSystemInfo("URL", BaseTest.getProperty("SITE_URL"));
		extent.setSystemInfo("Platform", System.getProperty("os.name"));
		extent.setSystemInfo("URL", properties.getProperty("SITE_URL"));
		extent.setSystemInfo("Browser",properties.getProperty("BROWSER_TYPE"));
		
		return extent;
	}
	
}