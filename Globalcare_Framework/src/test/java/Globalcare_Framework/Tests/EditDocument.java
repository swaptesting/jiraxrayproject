package Globalcare_Framework.Tests;

import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

import Globalcare_Framework.Pages.E_LibraryPage;
import Globalcare_Framework.Pages.LoginPage;
import Globalcare_Framework.Pages.RegistrationRequestPage;
import Globalcare_Framework.TestComponents.BaseTest;
import io.github.bonigarcia.wdm.WebDriverManager;

public class EditDocument extends BaseTest{
	private ExtentReports extent;
	private ExtentTest test;
	

	@Test
	public void editDocument() throws Exception{
		
	LoginPage loginpage=new LoginPage(driver);
	RegistrationRequestPage registrationRequestPage=new RegistrationRequestPage(driver);
	loginpage.navigateToURL(getProperty("SITE_URL"));
	loginpage.loginWithExcelData(filePath,sheetname);
	Thread.sleep(3000);
	registrationRequestPage.VerifyRegistrationPageTitle();
	registrationRequestPage.clickOnELibraryLink();
	Thread.sleep(5000);
	E_LibraryPage epage=new E_LibraryPage(driver);
	epage.clickOnTheEditLink();
	Thread.sleep(2000);
	epage.editLanguage(filePath,sheetname);
	Thread.sleep(3000);
	
	

}

}
