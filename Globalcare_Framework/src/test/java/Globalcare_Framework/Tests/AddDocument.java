package Globalcare_Framework.Tests;

import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import Globalcare_Framework.Pages.E_LibraryPage;
import Globalcare_Framework.Pages.LoginPage;
import Globalcare_Framework.Pages.RegistrationRequestPage;
import Globalcare_Framework.TestComponents.BaseTest;
import io.github.bonigarcia.wdm.WebDriverManager;

public class AddDocument extends BaseTest{
	

	@Test
	public void addDocuments() throws Exception{
	LoginPage loginpage=new LoginPage(driver);
	RegistrationRequestPage registrationRequestPage=new RegistrationRequestPage(driver);
	loginpage.navigateToURL(getProperty("SITE_URL"));
	loginpage.loginWithExcelData(filePath,sheetname);
	Thread.sleep(3000);
	registrationRequestPage.VerifyRegistrationPageTitle();
	registrationRequestPage.clickOnELibraryLink();
	Thread.sleep(3000);
	E_LibraryPage epage=new E_LibraryPage(driver);
	epage.clickOnaddNewDocumentLink();
	Thread.sleep(2000);	
	epage.enterDataforAddDocument(filePath,sheetname);
	Thread.sleep(3000);
	epage.verifyTheDocumentID();
	Thread.sleep(5000);
}

}
