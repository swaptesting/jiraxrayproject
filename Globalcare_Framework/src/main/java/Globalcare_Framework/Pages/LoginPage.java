package Globalcare_Framework.Pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Globalcare_Framework.abstractComponents.AbstractComponent;

public class LoginPage extends AbstractComponent{
	WebDriver driver;

	public LoginPage(WebDriver driver)
	{
		super(driver);
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
		
	
	@FindBy(id="P9999_USERNAME")
	WebElement username;
	
	@FindBy(id="P9999_PASSWORD")
	WebElement password;
	
	@FindBy(id="B1317108607847676431")
	WebElement submit;
	public void navigateToURL(String url) {
	driver.get(url);
		}
	
	
	public  void loginWithValidCredential(String uname,String pword)
	{
		username.sendKeys(uname);
		password.sendKeys(pword);
		//E_LibraryPage epage=new E_LibraryPage(driver);
		submit.click();
		
		
		
	}
	private String Screenshotfilepath=System.getProperty("user.dir")+"//reports";

	public void loginWithExcelData(String filepath, String sheetName) throws IOException, Exception {
		Object[][] loginData = readExcelData(filepath,sheetName);
		for(Object[] rowData : loginData) {
			String uname=rowData[0].toString();
			String pword=rowData[1].toString();
			username.sendKeys(uname);
			password.sendKeys(pword);
			submit.click();
			System.out.println(System.getProperty("user.dir"));
			//
			captureScreenshot(driver,Screenshotfilepath,"Login");
			Thread.sleep(4000);
			
		}
	}
	
	public void loginWithInvalidData(String filepath, String sheetName) throws Exception {
		Object[][] loginData = readExcelData(filepath,sheetName);
		for(Object[] rowData : loginData) {
			String uname=rowData[0].toString();
			String pword=rowData[1].toString();
			username.sendKeys(uname);
			password.sendKeys(pword);
			submit.click();
			Thread.sleep(3000);
			
			
		}
	}

}
