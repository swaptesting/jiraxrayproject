package Globalcare_Framework.Pages;



import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import Globalcare_Framework.abstractComponents.AbstractComponent;

public class E_LibraryPage extends AbstractComponent{
	WebDriver driver;

	public E_LibraryPage(WebDriver driver) {
		super(driver);
		this.driver=driver;
		PageFactory.initElements(driver, this);
		
	}
	@FindBy(xpath="(//ul[@role='none']/li)[3]")
	WebElement addNewDocumentLinks;
	
	
	@FindBy(id="B41838055149682330")
	WebElement addNewDocumentLink;
	
	@FindBy(xpath="//span[@id='ui-id-10']")
	WebElement add_Edit_PopUpBox;
	
	@FindBy(xpath="//span[@id='P18_ST_ELIB_DOC_ID_DISPLAY']")
	WebElement documentid_Txt;
	
	@FindBy(xpath="//select[@id='P18_ST_ELIB_DOC_TYPE']")
	WebElement docType_dropdown;
	
	@FindBy(xpath="//select[@id='P18_ST_ELIB_YEAR']")
	WebElement year_dropdown;
	
	@FindBy(xpath="//input[@id='P18_ST_ELIB_TITLE']")
	WebElement title_Textbox;
	
	@FindBy(xpath="//select[@id='P18_ST_ELIB_LANGUAGE']")
	WebElement languag_dropdown;
	
	@FindBy(xpath="//div[@id='P18_FILE_PDF_GROUP']/input")
	WebElement addfilePDF_btn;
			
	@FindBy(xpath="//div[@id='P18_PREV_IMAGE_GROUP']/input")
	WebElement previewimage_btn;
	
	@FindBy(id="B42301032496689803")
	WebElement save_btn;
	
	@FindBy(xpath="//label[@id='P18_ST_ELIB_DOC_TYPE_LABEL']")
	WebElement docTyple;
	
	@FindBy(id="41836207265682312_orig")
	WebElement table;

	@FindBy(xpath="//table[@id='41836207265682312_orig']/tbody/tr[2]/td[1]")
	WebElement edit_link;

	@FindBy(xpath="//button[@id='B42877294187318112']")
	WebElement savebtn;
	
	private String documentID;
	
	
	
	
	public void clickOnaddNewDocumentLink() {
		try {
			waitForWebElementToAppear(addNewDocumentLink);
			addNewDocumentLink.click();
	}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	public void getDocumentID() {
		try {
			waitForWebElementToAppear(documentid_Txt);
			 documentID=documentid_Txt.getText();
			System.out.println("Generated document id is for the current Execution  :"+documentID);
		}
		catch(Exception e) {
		e.printStackTrace();
	}
	}
	private String Screenshotfilepath=System.getProperty("user.dir")+"//reports";

	public void enterDataforAddDocument(String filepath, String sheetname) throws Exception {
	//waitForWebElementToAppear(docType_dropdown);
	//String path=System.getProperty("user.dir");
		Thread.sleep(5000);
	
		Object[][] loginData = readExcelData(filepath,sheetname);
		for(Object[] rowData : loginData) {
			String docType=rowData[2].toString();
			String year=rowData[3].toString();
			String title=rowData[4].toString();
			String language=rowData[5].toString();
			String addPDF=rowData[6].toString();
			String finalPdffilepath=System.getProperty("user.dir")+addPDF;
			System.out.println(finalPdffilepath);
			String previewFile=rowData[7].toString();
			String finalPNGfilePath=System.getProperty("user.dir")+previewFile;
			driver.switchTo().frame(0);
			getDocumentID();
			waitForWebElementToAppear(docType_dropdown);
			docType_dropdown.sendKeys(docType);
			waitForWebElementToAppear(docTyple);
			docTyple.click();
			waitForWebElementToAppear(year_dropdown);
			year_dropdown.sendKeys(year);
			title_Textbox.sendKeys(title);
			languag_dropdown.sendKeys(language);
			//waitForWebElementToAppear(addfilePDF_btn);
			Thread.sleep(3000);
			addfilePDF_btn.sendKeys(finalPdffilepath);
			Thread.sleep(5000);
			previewimage_btn.sendKeys(finalPNGfilePath);
			Thread.sleep(5000);
			captureScreenshot(driver,Screenshotfilepath,"AddDoc");
			save_btn.click();
			Thread.sleep(2000);
			driver.switchTo().defaultContent();
		
		}
	}
			
//waitForWebElementToClickable(addfilePDF_btn);
//((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",addfilePDF_btn);
//Actions action=new Actions(driver);
//action.moveToElement(addfilePDF_btn).click().build().perform();
//uploadFileWithRobot(finalPdffilepath);
//action.sendKeys(finalPdffilepath).click().build().perform();;
//File file=new File(finalPdffilepath);
//System.out.println(file.getAbsolutePath());
		
		
//	public void verifyTheDocumentID() {
//		List<WebElement> rows=table.findElements(By.tagName("tr"));
//		for(WebElement row:rows){
//		List<WebElement> cols=table.findElements(By.tagName("td"));
//		for(WebElement col: cols) {
//			String allDocument=col.getText();
//			System.out.println("Document IDs are  "+ allDocument);
//			assertTrue(allDocument.contains(documentID));
//		}
//			
//	}
	public void verifyTheDocumentID() throws IOException {
		String documentIDNo=driver.findElement(By.xpath("//table[@id='41836207265682312_orig']/tbody/tr[2]/td[2]")).getText();
		Assert.assertEquals(documentIDNo, documentID);
		captureScreenshot(driver,Screenshotfilepath,"GeneratedID");
				
		}
	
public void clickOnTheEditLink()  {
	waitForWebElementToAppear(edit_link);
	edit_link.click();
	captureScreenshot(driver,Screenshotfilepath,"GeneratedID");
			
	}
public void editLanguage(String filepath, String sheetname) throws Exception {
			Thread.sleep(2000);
	
		Object[][] loginData = readExcelData(filepath,sheetname);
		for(Object[] rowData : loginData) {
			String language=rowData[8].toString();
			driver.switchTo().frame(0);
			waitForWebElementToAppear(docType_dropdown);
			//languag_dropdown.clear();
			languag_dropdown.sendKeys(language);
			waitForWebElementToAppear(savebtn);
			savebtn.click();
			captureScreenshot(driver,Screenshotfilepath,"EditLanguage");
			Thread.sleep(2000);
		
			driver.switchTo().defaultContent();
	
}
}
}



