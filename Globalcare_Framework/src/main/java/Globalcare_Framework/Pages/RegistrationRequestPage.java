package Globalcare_Framework.Pages;




import java.time.Duration;

//import static org.junit.Assert.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Globalcare_Framework.abstractComponents.AbstractComponent;

public class RegistrationRequestPage  {
WebDriver driver;
	
	public RegistrationRequestPage(WebDriver driver)
	{
		//sssuper(driver);
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	//driver.findElement(By.xpath("(//ul[@role='none']/li)[3]")).click();
	@FindBy(xpath="(//*[text()='Registration requests'])[3]")
	WebElement registrationrequestTitle;
	
	@FindBy(xpath="(//ul[@role='none']/li)[3]")
	WebElement eLibrarylink;
	
	String pageTitle="Registration requests";
	public void verifyRegistrationrequestPageTitle() {
	try {				
	Assert.assertEquals(registrationrequestTitle.getText(), "Registration requests");
	}catch(Exception e){
		e.printStackTrace();
		
	}
	}
	public void clickOnELibraryLink() {
		try {
			
		eLibrarylink.click();
	}
		catch(Exception e)
		{
			e.printStackTrace();
					}
	}
	
	//WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
	//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//ul[@role='none']/li)[3]")));
	
	public void VerifyRegistrationPageTitle() {
		Boolean match=registrationrequestTitle.getText().equalsIgnoreCase(pageTitle);
		//return match;
		Assert.assertTrue(match);
		
	}
}
