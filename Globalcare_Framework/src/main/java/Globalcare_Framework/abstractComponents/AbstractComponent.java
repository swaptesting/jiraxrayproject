package Globalcare_Framework.abstractComponents;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AbstractComponent {
	WebDriver driver;

	public AbstractComponent(WebDriver driver) {
		this.driver=driver;
		// TODO Auto-generated constructor stub
	}	
	
	public void waitForWebElementToAppear(By findBy) {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
		wait.until(ExpectedConditions.visibilityOfElementLocated(findBy));
		
	}
	public void waitForWebElementToVisible(WebElement findBy) {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));
		wait.until(ExpectedConditions.elementToBeSelected(findBy));
		
	}
	public void waitForWebElementToClickable(WebElement findBy) {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));
		wait.until(ExpectedConditions.elementToBeClickable(findBy));
		
	}
	public void waitForWebElementToAppear(WebElement findBy) {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));
		wait.until(ExpectedConditions.visibilityOf(findBy));
		
	}
	
	protected  Object[][] readExcelData(String filePath, String sheetName) {
	    Object[][] data = null;

	    try (FileInputStream fis = new FileInputStream(new File(filePath));
	    		Workbook workbook = new XSSFWorkbook(fis)) {

	        Sheet sheet = workbook.getSheet(sheetName);
	        int rowCount = sheet.getPhysicalNumberOfRows();
	        int colCount = sheet.getRow(0).getPhysicalNumberOfCells();

	        data = new Object[rowCount - 1][colCount];

	        for (int i = 1; i < rowCount; i++) {
	            Row row = sheet.getRow(i);
	            for (int j = 0; j < colCount; j++) {
	                Cell cell = row.getCell(j);
	                data[i - 1][j] = getCellValue(cell);
	            }
	        }

	    } catch (IOException e) {
	        e.printStackTrace();
	    }

	    return data;
	}

	private  Object getCellValue(Cell cell) {
	    switch (cell.getCellType()) {
	        case STRING:
	            return cell.getStringCellValue();
	        case NUMERIC:
	            if (DateUtil.isCellDateFormatted(cell)) {
	                return cell.getDateCellValue();
	            } else {
	                return cell.getNumericCellValue();
	            }
	        case BOOLEAN:
	            return cell.getBooleanCellValue();
	        case FORMULA:
	            return cell.getCellFormula();
	        case BLANK:
	            return "";
	        default:
	            return null;
	    }
	}

public static void uploadFileWithRobot(String filePath) throws AWTException, Exception {
	Robot robot= new Robot();
	for(char c: filePath.toCharArray()) {
		robot.keyPress(KeyEvent.getExtendedKeyCodeForChar(c));
		robot.keyRelease(KeyEvent.getExtendedKeyCodeForChar(c));
	}
	robot.keyPress(KeyEvent.VK_ENTER);
	robot.keyRelease(KeyEvent.VK_ENTER);
	Thread.sleep(2000);
	
	}
public static void captureScreenshot(WebDriver driver, String outputPath, String fileName) {

    try {
           TakesScreenshot ts = (TakesScreenshot) driver;
           
        File source = ts.getScreenshotAs(OutputType.FILE);
              new File(outputPath).mkdirs();
               String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
              String filePath = outputPath + File.separator + fileName + "_" + timestamp + ".png";
              FileHandler.copy(source, new File(filePath));

        System.out.println("Screenshot captured: " + filePath);

    } catch (IOException e) {

        e.printStackTrace();

    }

}


}

